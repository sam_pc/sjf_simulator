//imports
const Dialogs = require('dialogs');
const dialogs = Dialogs();
const {ctrl_runSimulator, ctrl_fillData} = require("../../controller/ctrl_init");

// variables
let elmnt_processesList = document.getElementById("processesList"),
    elmnt_userName = document.getElementById("userName"),
    pidValue = 2201, pidRemoved = [];

// functions
const iconInfoClick = () => {
  alert(
    `
    ALGORITMO SJF 
    
    El algoritmo SJF (Shortest-Job-First) se basa en los ciclos de vida de los procesos, los cuales transcurren en dos etapas o periodos que son: ciclos de CPU y ciclos de entrada/salida, también conocidos por ráfagas.
    
    La palabra Shortest (el más corto) se refiere al proceso que tenga el próximo ciclo de CPU más corto. La idea es escoger entre todos los procesos listos el que tenga su próximo ciclo de CPU más pequeño.

    CARACTERISTICAS
  * El proceso en CPU es desalojado si llega a la cola con duración más corta.
  * Minimiza el tiempo de espera medio.
  * Riesgo de bloqueo de los procesos de larga duración.
  
  VENTAJAS
  * Asocia a cada proceso un tiempo aproximado de utilización de CPU.
  * Asigna la CPU al proceso con menor tiempo asociado.
  * Cuando un proceso consigue la CPU la conserva hasta que decide liberarla.
  * Es de gran utilidad en sistemas operativos que funcionan por lotes.

  DESVENTAJAS
  * No se puede usar en cualquier tipo de sistema operativo, solo en los que 
    funcionen por medio de lotes.
  * La dificultad a la hora de la estimación del tiempo de utilización de CPU 
    por parte de cada proceso.
`);
}

const iconDeleteAllClick = (e) => {
  e.preventDefault();
  if (elmnt_processesList.innerHTML != " ") {
    dialogs.confirm("Do you want to delete all processes?", (ok) => { (ok)? elmnt_processesList.innerHTML=" ": null}
    );
  }else{
    dialogs.alert("There are no processes to delete!");
  }
};

const iconUserClick = (e) => {
  e.preventDefault();
  let txt_userName = elmnt_userName.innerText;
  dialogs.prompt('Enter Username', txt_userName, ok => {
    if (ok !== undefined) {
      if(ok !== txt_userName){
        if (ok.length >= 3) { elmnt_userName.innerText = ok; }
        else{ dialogs.alert("Minimum length is 3");}
      }else{
        dialogs.alert("User is already used")
      }
    }
  });
}

const pDeleteClick = (e) => {
  e.preventDefault();
  if (e.target.classList.contains("pDelete")) {
    pidRemoved.push(e.target.parentElement.childNodes[3].childNodes[1].value);
    e.target.parentElement.remove();
  }
}

const iconAddClick = (e) => {
  e.preventDefault();
  let _pidValue = (pidRemoved.length === 0) ? pidValue : pidRemoved.shift();
  let html =
    `
    <span class=" pDelete"> x </span>
    <label> PID 
      <input type="text" disabled="true" value="${_pidValue}" name="pId" id="pId">
    </label>
    <label> User 
      <input type="text" disabled="true" value="${elmnt_userName.innerText}" name="pUser" id="pUser"> 
    </label>
    <label> Name 
      <input type="text" name="pName" id="pName">
    </label>
    <label> Duration
      <input type="number" min="0" name="pDuration" id="pDuration"> 
    </label>
    <label> Arribal 
      <input type="number" min="0" name="pArribal" id="pArribal">
    </label>
    `;
  let elmnt_process = document.createElement("DIV");
  elmnt_process.innerHTML=html; elmnt_process.classList.add("list__process");
  elmnt_process.addEventListener("click", pDeleteClick)
  elmnt_processesList.appendChild(elmnt_process);
  (_pidValue === pidValue) ? pidValue++ : null;
};

const iconPlayClick = (e) => {
  e.preventDefault();
  ctrl_fillData();
  ctrl_runSimulator();
}

// events
document.getElementById("iconInfo").addEventListener("click", iconInfoClick);
document.getElementById("iconDeleteAll").addEventListener("click", iconDeleteAllClick);
document.getElementById("iconUser").addEventListener("click", iconUserClick);
document.getElementById("iconAdd").addEventListener("click", iconAddClick );
document.getElementById("iconPlay").addEventListener("click", iconPlayClick);