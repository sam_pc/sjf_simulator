// imports
const {remote} = require("electron");
let sjfData;
// elements
const executionProcess = document.getElementById('executionProcess')
      executionTime = document.getElementById('executionTime'),
      tableResults= document.getElementById("tblResults"),
      buttons = document.getElementById('buttons'),
      tableCalcs = document.getElementById("tblCalcs").
      animation = document.getElementById('animation');

// functions
const newSimulationClick = (e) => {
  e.preventDefault();
  remote.getGlobal("shareObject").sjfData = "";
  remote.getCurrentWindow().loadFile(__dirname + "../../init/index.html");
}

const replaySimulationClick = (e) => {
  e.preventDefault();
  buttons.classList.add("none");
  tableResults.classList.add("none");
  tableCalcs.classList.add("none");
  init();
}

let resultsTable = (rawData) => {
  let data = rawData.sort(function(p1,p2) {return p1.position - p2.position});
  tableResults.classList.remove("none");
  data.filter((process) => process.name !== undefined).forEach(process => {
    let tableBody = document.getElementById("tableBody");
    tableBody.innerHTML +=
      `
      <tr>
        <td>${process.pid}</td>
        <td>${process.name}</td>
        <td>${process.user}</td>
        <td>${process.duration}</td>
        <td>${process.arrival}</td>
        <td>${process.timeWait}</td>
        <td>${process.timeDuration}</td>
       </tr>
    `;
  });

  let results = rawData[(rawData.length - 1)];
  tableCalcs.innerHTML =
  `<b>Wait time average: </b>${Math.round(results.waitTimeProm*100) /100} <b> - Duration time average: </b>${Math.round(results.durationTimeProm * 100)/100}`;
}

const printResults = () => {
  buttons.classList.remove("none");
  resultsTable(remote.getGlobal("shareObject").sjfData);
}

const exec = async (process) => await new Promise((resolve) => {
  let currentTime = 0;
  setTimeout(() => {
    executionProcess.innerHTML = `<b>Execute:  </b>${process.name}<b>  -  PID: </b>${process.pid}<b>  -  Arrival: </b> ${process.arrival}`;
    executionTime.innerHTML = `<b>Duration: </b> ${currentTime}`;
    setInterval(() => {
      if (currentTime < process.duration) { executionTime.innerHTML = `<b>Duration: </b> ${++currentTime}`; }
      else { clearInterval(this); resolve(); }
    }, 1500);
  }, 500);
  setTimeout(() => { animation.classList.remove("none"); }, 1000);
});

const nextTurn =  async () =>{
  if (sjfData.length > 0) { 
    await exec(sjfData.shift())
    animation.classList.add("none");
    executionProcess.innerHTML = ""; executionTime.innerHTML = "";
    nextTurn();
  }else{ printResults();}
}

const init = () => {
  sjfData = remote.getGlobal("shareObject").sjfData.filter((process) => process.name !== undefined)
                                                   .sort(function(p1,p2) {return p1.position - p2.position});
  nextTurn();
}

// event
document.getElementById("newSimulation").addEventListener("click", newSimulationClick);
document.getElementById("replaySimulation").addEventListener("click", replaySimulationClick);
document.addEventListener("DOMContentLoaded", init);