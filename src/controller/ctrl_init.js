// imports
const {calculosSJF} = require("./sjf");
const {Process} = require("../model/md_process");
const {remote} = require("electron");

// variables
processesList = [];
  
// functions
let sjfDataExample = () =>{
  let dataExample = []
  dataExample.push(new Process(22001, "administrator", "GIMP",3,2));
  dataExample.push(new Process(22002, "administrator", "DIA",1,4));
  dataExample.push(new Process(22003, "administrator", "JAVA",3,0));
  dataExample.push(new Process(22004, "administrator", "MARIADB",4,1));
  dataExample.push(new Process(22005, "administrator", "HTOP",2,3));
  
  return dataExample
}

const ctrl_runSimulator = () => {
  remote.getGlobal("shareObject").sjfData = calculosSJF(sjfDataExample()); //example
  // remote.getGlobal("shareObject").sjfData = calculosSJF(processesList)// original
  remote.getCurrentWindow().loadFile(__dirname + "/../view/simulator/index.html");
  
}

const ctrl_dataVerification = (elmnt_data) => {
  let input = elmnt_data.childNodes[1];
  if (input.value != "") { return input.value }
  else{processesList = []; throw("All inputs will be filled");  }
}

const ctrl_addToProcessList = (elmnt_process) => {
  let data_process = [];
  [...elmnt_process.childNodes].forEach(elmnt_data => {
    if (elmnt_data.tagName === "LABEL"){
      data_process.push(ctrl_dataVerification(elmnt_data));
    }
  });
  processesList.push(new Process(data_process[0], data_process[1], data_process[2], data_process[3], data_process[4]));
}

const ctrl_fillData = () => {
   // obtain process_data of every process added
   if (elmnt_processesList.childNodes.length > 1) {
    [...elmnt_processesList.childNodes].forEach(elmnt_process => {
      if (elmnt_process.classList != undefined) {
        try { ctrl_addToProcessList(elmnt_process) } 
        catch (error) { dialogs.alert(error); return} }    
    });
   }else{
     dialogs.alert("There are no processes to execute");
   }
  
}

module.exports = {ctrl_runSimulator, ctrl_fillData}