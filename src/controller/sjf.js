var procesos; 
var retornoActual = 0;
var posicionActual = 1;
var tiempoRetornoTotal = 0;
var tiempoEsperaTotal=0; 

function calculosSJF (procesosRecibidos) {
  procesos = [...procesosRecibidos];
  while (procesos.length !== 0) {
    var yaLlegaron = obtenerLosQueYaLlegaron();
    var elQueLeToca = obtenerElQueToca(yaLlegaron);
    retornoActual += elQueLeToca.duration;
    insertarNuevosDatos(procesosRecibidos, elQueLeToca);
  }
  return calculos(procesosRecibidos);

}

function obtenerElQueToca(yallegaron) {

  var yallegaronOrdenada = yallegaron.sort(function(p1,p2) {return p1.duration - p2.duration}); 
  //ordena de menor a payor por rafagas
  
  var toca = procesos.splice(procesos.indexOf(yallegaronOrdenada[0]),1)[0];
  //el cero del final ya que devuelve un arrego con le valor eliminado
  return  toca;
}

function calculos(procesosRecibidos) {
  var totalProcesos = procesosRecibidos.length;
  var tiempoEsperaPromedio = tiempoEsperaTotal / totalProcesos;
  var tiempoRetornoPomedio = tiempoRetornoTotal / totalProcesos;
  var calculos = 
  {
    waitTimeTotal: tiempoEsperaTotal,
    durationTimeTotal: retornoActual,
    waitTimeProm: tiempoEsperaPromedio,
    durationTimeProm: tiempoRetornoPomedio
  }
  procesosRecibidos.push(calculos);
  return procesosRecibidos;
}

function obtenerLosQueYaLlegaron() {
  var procesosQueYaLlegaron = [];
  for (var  i = 0; i < procesos.length; i++) {
    var proceso = procesos[i];
    if (proceso.arrival <= retornoActual) {
      procesosQueYaLlegaron.push(proceso);
    }
  }
  return procesosQueYaLlegaron;
}

function insertarNuevosDatos(procesosRecibidos, elQueLeToca) {
  for (var i = 0;  i < procesosRecibidos.length; i++) {
    var proceso = procesosRecibidos[i];
    if (proceso === elQueLeToca) {
      proceso.timeDuration = retornoActual;
      var tiempoEspera = (retornoActual - proceso.duration) - proceso.arrival;
      proceso.timeWait = tiempoEspera
      tiempoEsperaTotal += tiempoEspera;
      tiempoRetornoTotal += retornoActual;
      proceso.position = posicionActual;
      posicionActual++;
    }
  }
}

module.exports = {calculosSJF}